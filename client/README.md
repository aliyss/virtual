# wp-switch

Allow website owners to use `wp-switch-up-by-user` and `wp-switch-down-by-user` scripts to start and stop their website from a simple GUI.
