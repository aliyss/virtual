# rss.netig.net

## Installation

Start FreshRSS :
```
$ sudo docker compose up -d
```

Once started, go to https://rss.netig.net, at database configuration choose MySQL database and then :

* Host is `rss.netig.net_db`
* Database username is `freshrss`
* Password is what you have set in `freshrss.pass` file
* Database is `freshrss`
* Let tables prefix field empty

The next step is the main user creation `admin` is a good choice.

Configure mail sending by editing `/var/local/rss.netig.net/data/config.php`, this way :
```
  'mailer' => 'smtp',
  'smtp' =>
  array (
    'hostname' => 'netig.net', // the domain used in the Message-ID header
    'host' => 'mail.netig.net', // the SMTP server address
    'port' => 587,
    'auth' => true,
    'auth_type' => 'LOGIN', // 'CRAM-MD5', 'LOGIN', 'PLAIN', 'XOAUTH2' or ''
    'username' => 'admin',
    'password' => '<mail-password>',
    'secure' => 'tls', // '', 'ssl' or 'tls'
    'from' => 'admin@netig.net',
  ),
```

Since we are in main config file, if you see something useful to tweak, do it!

# Links

* https://freshrss.org/
