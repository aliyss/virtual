# WordPress

## Files upload limit

```
$ sudo mkdir -p /var/local/<exemple.tld>/php
```

To allow 8M uploads, create `/var/local/<exemple.tld>/php/uploads.ini` file with this content :
```
upload_max_filesize = 8M
post_max_size = 8M
```

## Reverse Proxy

> If you want to allow more or less upload than 8M, adapt this line :  `client_max_body_size 8m;` in `exemple.tld.conf`.

> The config file must not contain `$server_port` variable at `proxy_set_header        Host $host;` line.

## Extensions

If you don't need, you may remove :

* Akismet Anti-Spam
* Hello Dolly

For Redis support you may install :

* Redis Object Cache (By Till Krüss)

To send mails from WordPress :

* SMTP Mailer (By naa986)