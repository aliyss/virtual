# Services

## Overview

Services are organized by folders into `/virtual/services`, like this :

```
.
├── prod1.netig.lan
├── prod2.netig.lan
├── test.netig.lan
└── srvlist

```

One folder represents one virtual server.

Into folders they are distributed as `docker-compose.yml`, with environment variable spited into `.env` and `.pass` files. Typically it may look as in this example :

```
.
├── docker-compose.yml
├── pad.netig.net.env
├── pad.netig.net.pass
└── pad.netig.net.pass_template
```

> It may also contains a `README.md` file, with instructions to follow for this peculiar service. 

## Copy services into virtual servers

`.pass` file containing passwords are missing from this repository for obvious privacy issues. So the best way is to clone it into your personal computer, and use `.pass_template` files to make your own.

Then upload content of `/virtual/services` into `/srv` folder of destination servers using `sftp`.

Also restrict access to `/srv` as it contains passwords.
```
# chmod -R 700 /srv
```

## Launch services

*  If there are no data restored from an archive backup into `/var/local` to reuse, launch services one by one following any peculiar information that may be in there `README.md` file. Check logs and make sure they are running.

> For WordPress specific information take a look at WORDPRESS.md.

> For ns<1,2>.netig.net you have to build it before : `( cd /srv/prod<1,2,3>.netig.lan/ns<1,2>.netig.net && docker build -t nsd:latest . )`

* If there is some data to restored from an archive backup, extract it into `/var/local` folder. Using an archive of backups is mandatory as ownership and properties must be keeped. Then you could right away launch services one by one, or...

### Launch multiple services at once

To launch multiple services at once you could use `srv-up`, already copied into `/usr/local/bin` by `setup.sh`.

Put services to start into `srvlist`. \
Then start them with :
```
# srv-up
```

> This script could also be used to restart all services listed on `srvlist`. Restarting services will cause a few minutes interruption.

> `srv-down` is also available to down services.