# Docker memo

## Start/stop one service

Start command :
```
# cd /srv/prod<1,2,3>.netig.lan/<service_name>
# docker compose up -d
```

Stop command :
```
# cd /srv/prod<1,2,3>.netig.lan/<service_name>
# docker compose down
```


## Execute a shell inside a running container

```
$ sudo docker container exec -it <container> /bin/bash
```

## Clean up old stuff!

```
$ sudo docker system prune
```

## Run a test HTTPD

```
$ sudo docker run -dit --name <name> -p <port>:80 httpd:latest
```

## Check logs of a container

```
# docker container logs <container_name>
```
