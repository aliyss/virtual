# mail.netig.net

## Prerequisites

Necessary DNS records :

* netig.net 	MX 	mail.netig.net

* mail.netig.net 	A 	80.67.190.140

> Do not make AAAA record for mail.netig.net, otherwise your mailserver will not be able to recieve messages from Gmail (and maybe others).

## Installation

```
$ sudo docker compose up -d
$ sudo docker exec -ti mail.netig.net setup email add admin@netig.net
$ sudo docker exec -ti mail.netig.net setup alias add postmaster@netig.net admin@netig.net
```

Generate DKIM key (and for any added domain in the future).
```
$ sudo docker exec -ti mail.netig.net setup config dkim
```

You can see public key with this command :
```
$ sudo cat /var/local/mail.netig.net/config/opendkim/keys/netig.net/mail.txt
```

You may paste as is this into your DNS zone.

Register also SPF :

* netig.net 	TXT 	"v=spf1 mx ~all"

And finaly the DMARC record :

* _dmarc.netig.net 	TXT 	"v=DMARC1; p=none"

Then restart Docker Mail Server :
```
$ docker compose down
$ docker compose up -d
```

## Updating

```
$ sudo docker compose pull
$ sudo docker compose down
$ sudo docker compose up -d
```

# Links

* https://docker-mailserver.github.io/docker-mailserver/edge
