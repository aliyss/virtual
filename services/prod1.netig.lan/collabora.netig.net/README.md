# collabora.netig.net

## Prerequisites

Make sure to have TLS certs for `netig.net` including `cloud.netig.net` and `collabora.netig.net` subdomains.

## Installation

Copy `collabora.netig.net.conf` into `/etc/nginx/conf.d/` and restart `nginx.service`.
```
$ sudo systemctl restart nginx.service
```

Then into `/srv/thorin/collabora.netig.net` lauch collabora.
```
$ sudo docker compose up -d
```

Install Nextcloud Office from https://cloud.netig.net/index.php/settings/apps/office and enable it.

Go to this URL : https://cloud.netig.net/index.php/settings/admin/richdocuments
Select use our own server, and paste `https://collabora.netig.net/` into `URL (and port) of Collabora Online server` field, save it.
