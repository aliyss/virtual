# ns1.netig.net

NSD as an authoritative DNS name server to host delegated subdomains.

## Prerequisites

First register the following zone into your registrar DNS server web interface :

* ns1.netig.net 	A 	80.67.190.140
* ns1.netig.net 	AAAA 	2a00:5884:830a:0:0:0:0:1

## Installation

Generate the container image.
```
$ sudo docker build -t nsd:latest .
```

Create the `/var/local/ns1.netig.net/nsd.conf.d/zone.conf` config file, it contains a list of all zones.
Here an example, one zone section per delegated subdomain :
```
zone:
  name: sub.domain.tld
  zonefile: sub.domain.tld

zone:
  name: sub.other-domain.tld
  zonefile: sub.other-domain.tld
```

Then regiter the subdomains to delegate into your registrar DNS server web interface (for all subdomains to delegate), in this way :

* sub.domain.tld 	NS 	ns1.netig.net

After that for each delegated subdomain create a `/var/local/ns1.netig.net/zones/<sub.domain.tld>` file.
```
$ORIGIN <sub.domain.tld>.
$TTL 3600

; SOA RECORD
@                   IN               SOA                  ns1.netig.net. hostmaster.netig.net. (
                                     1679578861           ;Serial
                                     7200                 ;Refresh
                                     1800                 ;Retry
                                     1209600              ;Expire
                                     86400                ;Negative response caching TTL
)

; NAMESERVERS
@                   IN                NS                   ns1.netig.net.

; A RECORDS
@                   IN                A                    80.67.190.140
www                 IN                A                    80.67.190.140

; AAAA RECORDS
@                   IN                AAAA                 2a00:5884:830a:0:0:0:0:1
www                 IN                AAAA                 2a00:5884:830a:0:0:0:0:1

; MX RECORDS
@                   IN                MX        10         mail.<sub.domain.tld>.

; TXT RECORDS
```

> Make sure you have an email alias to receive messages from hostmaster@netig.net.

> If you have also `ns2.netig.net` running on another server you should add it to NAMESERVERS list. Use the same zone files.

You can ckeck your zones with :
```
$ sudo docker container exec -it ns1.netig.net nsd-checkzone <sub.domain.tld> /etc/nsd/zones/<sub.domain.tld>
```

You're good to run NSD now!
```
$ sudo docker compose up -d
```

Check here for DNS propagation : https://dnschecker.org
And here to easily get many infos about your managed domains https://intodns.com !

## Editing or creating new zone

When a zone is edited or newly created do not forget to down/up the `docker-compose.yml`.

# Links

* https://nlnetlabs.nl/projects/nsd/about
