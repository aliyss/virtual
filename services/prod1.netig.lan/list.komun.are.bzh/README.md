# list.komun.are.bzh

## Installation

Create the `/var/local/list.komun.are.bzh/config.toml` file.
Here an example of how it should be filled. Adapt <username>, <password> and <db_password>.

```
[app]
address = "0.0.0.0:80"
admin_username = "<username>"
admin_password = "<password>"

# Database.
[db]
host = "list.komun.are.bzh_db"
port = 5432
user = "listmonk"
password = "<db_password>"
database = "listmonk"
ssl_mode = "disable"
max_open = 25
max_idle = 25
max_lifetime = "300s"
```

Run the Postgres databse.
```
$ sudo docker compose up db -d
```

Then setup the database.
```
$ sudo docker compose run --rm app ./listmonk --install
```
(or `--upgrade` to upgrade an existing database)

Once done, simply launch listmonk with :
```
$ sudo docker compose up -d
```

# Links

* https://listmonk.app/docs/installation
