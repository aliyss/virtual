# cloud.netig.net

## Files upload limit

```
$ sudo mkdir -p /var/local/cloud.netig.net/php
```

To allow 512M uploads, create `/var/local/cloud.netig.net/php/uploads.ini` file with this content :
```
upload_max_filesize = 512M
post_max_size = 512M
```

## Reverse Proxy

See `cloud.netig.net.conf` for a working configuration.

> If you want to allow more or less upload than 512M, adapt this line :  `client_max_body_size 512m;` in `cloud.netig.net.conf`.

> The config file must not contain `$server_port` variable at `proxy_set_header        Host $host;` line.

Finally execute this command to add `'overwritehost' => 'cloud.netig.net',` to the nextcloud config.
```
# docker exec --user www-data -it cloud.netig.net php occ config:system:set overwritehost --value=cloud.netig.net
```

## Tips

To set the correct `default_phone_region`.
```
# docker exec --user www-data -it cloud.netig.net php occ config:system:set default_phone_region --value=FR
```

To disable the contact menu.
```
# docker exec --user www-data -it cloud.netig.net sed -i '/<div id="contactsmenu"><\/div>/d' core/templates/layout.user.php
```