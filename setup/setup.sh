#!/bin/bash

# Exit on most errors
set -e

# Update
apt -y update
apt -y upgrade

# Set hostname
echo
echo "What is machine hostname? (eg. prod1)"
read -p "> " hostname
hostnamectl set-hostname $hostname
sed -i "s/debian/$hostname/g" /etc/hosts

# Create first admin session
echo
echo "What is the username of the first administrator of this machine?"
read -p "> " admin_name
adduser $admin_name
apt install -y sudo
adduser $admin_name sudo

# Change root password
echo
echo "Type new root password."
passwd

# Install fail2ban
apt install -y fail2ban

# Install firewall
apt install -y ufw
ufw default deny
ufw allow 22
ufw allow 80
ufw allow 443
ufw allow 25
ufw allow 143
ufw allow 587
ufw allow 993
ufw allow 53
ufw enable
ufw reload
        
# Install useful utilities
apt install -y htop tree

## Install docker

# Update the apt package index and install packages to allow apt to use a repository over HTTPS
apt update -y
apt install -y \
  ca-certificates \
  curl \
  gnupg

# Add Docker’s official GPG key
mkdir -m 0755 -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg

# Use the following command to set up the repository
echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  tee /etc/apt/sources.list.d/docker.list > /dev/null

# Update the apt package index
apt update -y

# Install the latest version
apt install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

# Copy sudo extra permissions file
cp config/etc/sudoers.d/permissions /etc/sudoers.d/

# Copy scripts
cp scripts/usr/local/bin/* /usr/local/bin/

# Reboot
systemctl reboot
