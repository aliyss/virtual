# Setup

## Preparing a prod<1,2>.netig.lan virtual machine

First install Debian stable, keep `debian` as hostname it will be changed after.

Login as root, then :

```
# wget https://gitea.com/netig/vs/srv/archive/main.tar.gz
# tar -xvzf main.tar.gz
# cd vs/setup/
```

And finally execute `setup.sh` script to setup a fresh VM as NETig server.
```
# ./setup.sh
```

When finished, machine will reboot. Once started you will be able to connect with your user by `ssh`. Your user has admin capabilities with `sudo`.

## OpenVPN (Optional)

If you need a VPN to get your public IP, install and set up OpenVPN.

## wp-switch users (Optional)

If `wp-switch` tool is used you could automate users creation with `wp-switch_users.sh` script, just list usernames (websites domains) in `wp-switch_list` and execute the script. Don't forget to define user's passwords after that with `# passwd <user>` command.

## Services

Containerized services are not copied by `setup.sh`, you have to it manually, give a look at `vs/srv/README.md`.