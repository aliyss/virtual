#!/bin/bash

# Exit on most errors
set -e

# Add website owners
if [ -s userslist ]; then
  while read line
  do
    ( adduser --disabled-password --no-create-home --shell /bin/rbash --force-badname --gecos "$line" $line )
  done < userslist
fi
