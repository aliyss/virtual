# virtual-servers

This repository contains all resources about NETig virtual servers :

* setup : Contains an helper script to setup a virtuals server with needed scripts and config files
* srv : All NETig services as Docker containers
* client : Contains end user tools

## Overview

Services are entirely provided as docker container, itself powered by two bare metal machines with Proxmox VE.

Virtual server used to power NETig services are :

* prod1.netig.lan
* prod2.netig.lan

Test server is :

* test.netig.lan

### prod1.netig.lan

Machine propulsing ns1 and important services such as cloud and websites.

* rp1
* ns1.netig.net
* mailserver
* webmail.netig.net
* cloud.netig.net
* collabora.netig.net 
* mailing lists
* websites

### prod2.netig.lan

Machine for ns2.netig.lan and free access services plus monitoring and statistics tools.

> It would be interesting to host monitoring elsewhere.

* rp2
* ns2.netig.net
* calc.netig.net
* drop.netig.net
* pad.netig.net
* rss.netig.net
* status.netig.net
* stats.netig.net
